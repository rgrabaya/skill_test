<?php

class Route
{
    protected static $routes = array(
        'GET' => array(),
        'POST' => array()
    );

    /**
     * Load the routes file
     *
     * @param $file
     * @return Route
     */
    public static function load($file)
    {
        $route = new static;
        require $file;

        return $route;
    }

    /**
     * GET request
     *
     * @param  $uri
     * @param  $controller
     * @return void
     */
    public static function get($uri, $controller)
    {
        self::$routes['GET'][$uri] = $controller;
    }

    /**
     * POST request
     *
     * @param  $uri
     * @param  $controller
     * @return void
     */
    public static function post($uri, $controller)
    {
        self::$routes['POST'][$uri] = $controller;
    }

    /**
     * Direct uri http method
     *
     * @param $uri
     * @param $method
     * @return void
     */
    public function direct($uri, $method)
    {
        if (array_key_exists($uri, self::$routes[$method])) {
            return self::$routes[$method][$uri];
        }

        throw new Exception('No route found.');
    }
}
