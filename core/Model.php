<?php

class Model
{
    protected $database;

    public function __construct($database)
    {
        $this->database = $database;
    }

    /**
     * Creats a new instance of a model
     *
     * @param string $model
     * @return void
     */
    public function create($model)
    {
        require 'models/' . $model . '.php';

        return new $model($this->database);
    }

    public function db()
    {
        return $this->database;
    }
}
