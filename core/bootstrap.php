<?php
session_start();

require 'database/Connection.php';
require 'database/QueryBuilder.php';
require 'core/Route.php';
require 'http/Request.php';
require 'core/Model.php';

$config = require 'config.php';
$database = new QueryBuilder(Connection::create($config['database']));
$model = new Model($database);
