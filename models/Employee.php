<?php

/**
 * Class Employee
 *
 * This model represents an employee in our system.
 */
class Employee extends Model
{
    protected $table = 'employees';
    protected $fillable = array(
        'name',
        'gender',
        'phone_number',
        'type',
        'email',
        'password',
        'email_sent',
    );

    /**
     * @var bool
     */
    public $email_sent;

    /**
     * The unique identifier
     * @var int
     */
    public $id;

    /**
     * The employee's full name
     * @var string
     */
    public $name;

    /**
     * The employee's phone number
     * @var string
     */
    public $phone_number;

    /**
     * The employee's password.
     * @var string
     */
    public $password;

    /**
     * The employee's email address
     * @var string
     */
    public $email;

    /**
     * @var string
     * @see self::TYPE_* constants
     */
    public $type;

    /**
     * Full time employee - works 40 hours week+
     */
    const TYPE_FULL_TIME = "full-time";

    /**
     * Part time employee - works < 40 hours week.
     */
    const TYPE_PART_TIME = "part-time";

    /**
     * Save method (this has been hacked to save to the database, but it should be presumed that this would happen in
     * an ORM of some sort - and that the ORM takes care of the DB connection, query, etc.)
     *
     * It should also be assumed that the save method will work nicely for inserting and saving updates to an already existing employee.
     *
     * @return void
     * @throws Exception if we had a problem saving.
     */
    public function save()
    {
        $updateId = false;

        if (!$this->id) {
            $this->database->insert($this->table, $this->getProperties());
            $updateId = true;
        } else {
            $this->database->update($this->table, $this->getProperties(), $this->id);
        }

        if ($updateId) {
            $this->id = $this->database->lastInsertId();
        }
    }

    private function getProperties()
    {
        $properties = array();
        $fillable = array_flip($this->fillable);

        foreach ($fillable as $key => $value) {
            $properties[$key] = $this->$key;
        }

        return $properties;
    }
}
