<?php

class EmployeeRepository
{
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Registers a new employee
     *
     * @param array $data
     * @return Employee
     */
    public function register($data)
    {
        $employee = $this->model->create('Employee');
        $employee->name = $data['name'];
        $employee->gender = $data['gender'];
        $employee->email = $data['email'];
        $employee->phone_number = $data['phone_number'];
        $employee->type = $data['type'];
        $generatedPassword = uniqid();
        $employee->password = sha1($generatedPassword);

        $employee->save();

        //auditing
        $timestamp = date("d/m/y h:i:s");
        $this->model->db()->insert('audit_log', array(
            'message' => $data['name'] . ' was added on ' . $timestamp
        ));

        //send comms
        mail($data['email'], "Thanks for registering", "Dear " . $data['name'] . ",\nThanks for registering with AwesomeCorp!! your password is $generatedPassword.\nYou can login at: http://www.awesomecorp.com/login.\nRegards,\nAwesomeCorp");

        $employee->email_sent = 1;
        $employee->save();

        $this->saveToReports($employee);

        return $employee;
    }

    private function saveToReports($employee)
    {
        $data = array();
        $data['id'] = $employee->id;
        $data['name'] = $employee->name;
        $data['email'] = $employee->email;

        // write CSV
        $fp = fopen('tmp/employee_report.csv', 'a');
        fputcsv($fp, $data);
        fclose($fp);
    }
}
