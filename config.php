<?php

return array(
    'database' => array(
        'connection' => 'mysql:host=192.168.66.6',
        'username' => 'root',
        'password' => 'root',
        'schema' => 'test',
        'options' => array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        )
    )
);
