<?php

class Connection
{
    /**
     * Creates a new connection to the database
     *
     * @param array $config
     * @return void
     */
    public static function create($config)
    {
        try {
            return new PDO(
                $config['connection'] . ';dbname=' . $config['schema'],
                $config['username'],
                $config['password'],
                $config['options']
            );
        } catch (PDOException $e) {
            die('Connection failed: ' . $e->getMessage());
        }
    }
}
