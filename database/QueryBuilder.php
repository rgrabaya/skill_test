<?php

class QueryBuilder
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Performs an insert query
     *
     * @param string $table
     * @param array $params
     * @return void
     */
    public function insert($table, $params)
    {
        $query = sprintf(
            'INSERT INTO %s (%s) values (%s)',
            $table,
            implode(', ', array_keys($params)),
            ':' . implode(', :', array_keys($params))
        );

        $statement = $this->pdo->prepare($query);
        $statement->execute($params);
    }

    /**
     * Performs an update query
     *
     * @param string $table
     * @param array $params
     * @return void
     */
    public function update($table, $params, $id)
    {
        $query = sprintf('UPDATE %s SET ', $table);

        foreach ($params as $key => $value) {
            $query .= "$key=:$key, ";
        }

        $query = rtrim($query, ", ");
        $query .= sprintf(' WHERE id = %s', $id);

        $statement = $this->pdo->prepare($query);
        $statement->execute($params);
    }

    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
}
