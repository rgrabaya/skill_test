<?php
require 'repositories/EmployeeRepository.php';

/**
 * For the purposes of the test, the way that the data comes in to the system is not relevant. Let's assume that
 * we have a REST API, and this is a POST request to the "new employee" endpoint. The data provided is the array
 * that has been specified here.
 */

//allowed data is "name", "number", "email" & "type"
$receivedData = array(
    "name" => "Bob Smith",
    "gender" => "male",
    "email" => "luke.zawadzki@astutepayroll.com",
    "phone_number" => "+61 430 131 409",
    "type" => "full-time"
);

//this simulates a call to the API.
var_dump(handle_API_Request($receivedData, $model));

/**
 * This is the processing code for the API.
 */
function handle_API_Request($data, $model)
{
    try {
        $repo = new EmployeeRepository($model);
        $employee = $repo->register($data);

        return array(
            "status" => "success",
            "message" => $employee->id . " was added"
        );
    } catch (exception $e) {
        return array(
            "status" => "error",
            "message" => $e->getMessage()
        );
    }
}
