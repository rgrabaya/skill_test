<?php
require 'repositories/EmployeeRepository.php';

if ($_POST) {
    try {
        $repo = new EmployeeRepository($model);
        $employee = $repo->register($_POST);

        $_SESSION["logged_in_user_id"] = $employee->id;

        header('Location: dashboard');
    } catch (Exception $e) {
        $_SESSION['error'] = $e->getMessage();
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}
