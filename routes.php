<?php

Route::get('', 'http/controllers/index.php');
Route::get('dashboard', 'http/controllers/dashboard.php');
Route::get('new', 'http/controllers/create.php');
Route::post('new', 'http/controllers/store.php');

// for the purpose of this test, I just use a get method
Route::get('api/employees', 'http/controllers/api/employee.php');
