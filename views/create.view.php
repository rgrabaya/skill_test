<h1>Create New Employee</h1>
<form action="" method="post">
    <ul>
        <li>
            Employee Name:
            <input name="name" type="text" required />
        </li>
        <li>
            Gender
            <select name="gender">
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
        </li>
        <li>
            Phone Number:
            <input name="phone_number" type="text" required />
        </li>
        <li>
            Email:
            <input name="email" type="email" required />
        </li>
        <li>
            Employee Type:
            <select name="type">
                <option value="part-time">Part Time</option>
                <option value="full-time">Full Time</option>
            </select>
        </li>
    </ul>
    <input type="submit" value="Create">
    <?php
    if (isset($_SESSION['error'])) {
        echo '<p style="color: red"> ' . $_SESSION['error'] . '</p>';
        unset($_SESSION['error']);
    }
    ?>
</form>